package com.kfwebstandardl.beans;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Bean that provides the images that the content flow will display Creates an
 * ArrayList of ImageBenas with the image file name
 *
 * @author Ken Fogel
 */
@Named
@RequestScoped
public class ImagesView {

    private List<ImageBean> imageBeans;

    @PostConstruct
    public void init() {
        imageBeans = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            imageBeans.add(new ImageBean("image" + i + ".JPG", "image" + i));
        }
    }

    public List<ImageBean> getImagebeans() {
        return imageBeans;
    }
}
